# 20201125 Workshop
[Set up your playground](https://richard02110.signin.aws.amazon.com/console)

## 11/25 General Skill
- [Course overview + the shell](https://missing.csail.mit.edu/2020/course-shell/)
- [Shell Tools and Scripting](https://missing.csail.mit.edu/2020/shell-tools/)
- [Editors (Vim)](https://missing.csail.mit.edu/2020/editors/)
- [Data Wrangling](https://missing.csail.mit.edu/2020/data-wrangling/)
- [Command-line Environment](https://missing.csail.mit.edu/2020/command-line/)
- [Version Control (Git)](https://missing.csail.mit.edu/2020/version-control/)

## 11/26 AWS Knowledge
- IAM: User, Role, Group, Permission, and Policy
- AZ: Region and Zone
- VPC: Route Table, Subnet
- EC2: EBS, Security Group, AMI, Autoscaling, and ALB
- Storage and Data: Cloudfront, S3, and RDS

## 11/27 Challenge
- [S3 Game](http://s3game-level1.s3-website.us-east-2.amazonaws.com)
- [Finding and addressing Network Misconfigurations on AWS Overview](https://validating-network-reachability.awssecworkshops.com)
